package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"sync"
)

var audioFiles map[string]os.FileInfo

func hydrate() {
	audioFiles = make(map[string]os.FileInfo)
	listFiles()
}

var mu sync.Mutex

// TODO: make this func return files to caller
func listFiles() {
	mu.Lock()
	defer mu.Unlock()
	files, err := ioutil.ReadDir("./files")
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		audioFiles[f.Name()] = f
	}
}

func str(n string) string {
	s := strings.ReplaceAll(n, ".mp3", "")
	return strings.ReplaceAll(s, "-", " ")
}

func route(w http.ResponseWriter, r *http.Request) {
	if _, ok := r.URL.Query()["file"]; !ok {
		front(w, r)
	} else {
		play(w, r)
	}
}

func front(w http.ResponseWriter, r *http.Request) {
	// add volume links
	s := ""
	for i := 0; i <= 100; i += 5 {
		ii := strconv.Itoa(i)
		s += fmt.Sprintf(`<a href="/config?volume=%s">%s</a>&nbsp;`, ii, ii)
	}
	fmt.Fprintf(w, fmt.Sprintf(`<div>Volume:&nbsp;%s</div>`, s))

	// add file links
	listFiles()
	names := []string{}
	for name, _ := range audioFiles {
		names = append(names, name)
	}
	sort.Strings(names)
	for _, n := range names {
		fmt.Fprintf(w, `<h1><a href="/?file=%s">%s</a></h1>`, n, str(n))
	}
}

func play(w http.ResponseWriter, r *http.Request) {
	// TODO: return error in case of !ok
	if keys, ok := r.URL.Query()["file"]; ok {
		log.Printf("playing %s", keys[0])
		cmd := exec.Command("sh", "-c", fmt.Sprintf("/home/igor/gooplayer/trigger.sh %s", keys[0]))
		err := cmd.Run()
		if err != nil {
			log.Fatal(err)
		}
	}
	http.Redirect(w, r, "/", http.StatusFound)
	return
}

func config(w http.ResponseWriter, r *http.Request) {
	keys := r.URL.Query()
	if keys, ok := keys["volume"]; ok && len(keys) == 1 {
		log.Printf("setting volume to %s", keys[0])
		cmd := exec.Command("sh", "-c", fmt.Sprintf("/home/igor/gooplayer/config.sh volume %s", keys[0]))
		err := cmd.Run()
		if err != nil {
			log.Fatal(err)
		}
	}
	http.Redirect(w, r, "/", http.StatusFound)
	return
}

func main() {
	mu = sync.Mutex{}
	hydrate()
	http.HandleFunc("/", route)
	http.HandleFunc("/config", config)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
