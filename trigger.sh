#!/bin/bash

GOOLOG=/tmp/gooplayer.log
GOOPID=/tmp/gooplayer.pid
GOOTRACK="$1"

if [ -z "$1" ]; then
  GOOTRACK="waves"
fi

log() {
  prefix="[$(date)][trigger]"
  msg="$prefix $@"
  echo "$prefix $@" >> $GOOLOG
}

list_descendants ()
{
  local children=$(ps -o pid= --ppid "$1")

  for pid in $children
  do
    list_descendants "$pid"
  done

  echo "$children"
}

kill_family() {
  kill $(list_descendants $1)
  kill $1
}


if [ -f "$GOOPID" ]; then
  pid=$(cat $GOOPID)
  log "found existing player stopping at pid $pid"
  kill_family $pid || log "error stopping pid: $pid (exit code: $?)"
fi

# TODO: check if not running and exit with error
sh play.sh $GOOTRACK &
pid=$!
log "playing $1 on pid: $pid"

echo $pid > $GOOPID
