#!/bin/bash

GOOLOG=/tmp/gooplayer.log
TIMEOUT=2

log() {
  prefix="[$(date)][play]"
  echo "$prefix $@" >> $GOOLOG
}

log "starting player with $1"

while [ 1 ]; do
  AUDIODEV='bluez_sink.F4_4E_FD_28_E4_BE.a2dp_sink' play -q files/$1 2> /tmp/crash || log "play error"
  code=$?
  if [ $code -ne 0 ]; then
    log "play exit with code $code"
    exit 1
  fi
  sleep $TIMEOUT
done

