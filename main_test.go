package main

import "testing"

func Test_hydrate(t *testing.T) {
	hydrate()
	if _, ok := audioFiles["chopin.mp3"]; !ok {
		t.Errorf("expected chopin.mp3 to be set but it wasn't")
	}
}
