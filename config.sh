#!/bin/bash

GOOLOG="/tmp/gooplayer.log"
log() {
  prefix="[$(date)][config]"
  echo "$prefix $@" >> $GOOLOG
}

log "config"

volume() {
  # TODO: verify numeric value
  vol=$1
  if [ -z "$vol" ]; then
    vol="0"
  fi
  sink=$(pactl list sinks | grep 'Name: bluez_sink.F4_4E_FD_28_E4_BE.a2dp_sink' -B 2 | head -1 | sed 's/Sink #\([0-9]*\)/\1/g')
  log "setting volume $vol for sink $sink" 
  cmd="pactl set-sink-volume $sink '${vol}%'"
  eval $cmd
}

log "configuring player with '$@'"

case $1 in
"volume")
  volume $2
  ;;
*)
  log "unknown config command '$1'"
  ;;
esac
